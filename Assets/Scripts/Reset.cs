﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reset : MonoBehaviour
{
    public EndPointCollider endPointCollider;
    public Player player;
    public RespawnEnemys respawnEnemys;
    public EndPoint endPoint;
    public MyAudio myAudio;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (endPointCollider.isRestting == true)
        {
           endPointCollider.timer.secondsCount = 0;
        }
    }
    void OnTriggerEnter(Collider collision)
    {
        endPointCollider.resettingText.SetActive(false);
        endPointCollider.isRestting = false;
        endPointCollider.despawnBlockOnEnd.SetActive(true);
        endPointCollider.endPointCollider.GetComponent<BoxCollider>().enabled = true;
        respawnEnemys.respawnAllEnemysAndTargets();
        endPoint.resetNumbersOfEnemysAndTargets();
        player.health = 30;
        player.ammo = 15;
    }
    public void playerDied()
    {
        myAudio.playPlayerDeath();
        player.health = 30;
        player.ammo = 15;
        endPointCollider.timer.secondsCount = 0;
        respawnEnemys.respawnAllEnemysAndTargets();
        endPoint.resetNumbersOfEnemysAndTargets();
    }
}
