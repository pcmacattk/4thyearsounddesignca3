﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using TMPro;

public class EndPointCollider : MonoBehaviour
{
    public TextMeshProUGUI numberTime;
    public UIElements uIElements;
    public Timer timer;
    public GameObject parentTarget;
    public GameObject parentEnemy;
    public GameObject despawnBlockOnEnd;
    public GameObject despawnBlockOnEndSpawnPoint;
    public GameObject endPointCollider;
    public GameObject resettingText;
    int currentRunTime;
    int addtoTime = 0;
    int numofTargetsAlive;
    int numofEnemysAlive;
    public bool isRestting = false;
    // Start is called before the first frame update
    void Start()
    {
        resettingText.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnCollisionEnter(Collision collision)
    {
        //Check for a match with the specified name on any GameObject that collides with your GameObject
        if (collision.gameObject.transform.name == "Player")
        {
            despawnEndBlock();
            checkNumOfEnemysAlive();
            checkNumOfTargetsAlive();
            currentRunTime = (int)timer.secondsCount + addtoTime;
            checkBestTime();
            endPointCollider.GetComponent<BoxCollider>().enabled = false;
            resettingText.SetActive(true);
            isRestting = true;
        }

        
    }
    void checkBestTime()
    {
        if (currentRunTime < uIElements.bestTime)
        {
            uIElements.bestTime = currentRunTime;
            uIElements.updatebesttimestext();
        }
    }
    
    void checkNumOfTargetsAlive()
    {
        numofTargetsAlive = parentTarget.transform.childCount;
        if (numofTargetsAlive > 0)
        {
            addtoTime = numofTargetsAlive * 10;
        }
    }
    void checkNumOfEnemysAlive()
    {
        numofEnemysAlive = parentEnemy.transform.childCount;
        if (numofEnemysAlive > 0)
        {
            addtoTime = numofEnemysAlive * 15;
        }
    }
    void despawnEndBlock()
    {
        despawnBlockOnEnd.SetActive(false);
        despawnBlockOnEndSpawnPoint.SetActive(false);
    }
}
