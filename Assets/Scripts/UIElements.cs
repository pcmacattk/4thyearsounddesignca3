﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.IO;
public class UIElements : MonoBehaviour
{
    public MyAudio myAudio;
    public EndPoint endPoint;
    public Timer timer;
    public int bestTime = 500;
    public TextMeshProUGUI bestTimes;
    public TextMeshProUGUI enemysLeft;
    public TextMeshProUGUI targetsLeft;
    public TextMeshProUGUI damageTaken;
    public GameObject endPointCollider;

    // Start is called before the first frame update
    void Start()
    {
        bestTimes.text = bestTime.ToString();
        //endPointCollider.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //if ((endPoint.numOfEnemys) == 0 && endPoint.numOfTargets == 0)
        //{
        //    endPointCollider.SetActive(true);
        //}
        updatenumofenemy();
        updatenumoftargets();
    }

    public void updatenumofenemy()
    {
        enemysLeft.text = endPoint.numOfEnemys.ToString();
    }
    public void updatenumoftargets()
    {
        targetsLeft.text = endPoint.numOfTargets.ToString();
    }
    public void updatebesttimestext()
    {
        bestTimes.text = bestTime.ToString();
        myAudio.playguideBest();
    }


}
