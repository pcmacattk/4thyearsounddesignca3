﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserScript : MonoBehaviour
{
    public Player player;
    public GameObject laseremitter;
    public GameObject laser;
    public GameObject lasertarget;
    public float laserforce;
    public MyAudio myAudio;
    public InGameMenu inGameMenu;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update() // Some of this code is from a question asking about shooting found here https://forum.unity.com/threads/how-can-i-make-a-enemy-shoot-script.211639/ 
    {
        if (Input.GetButtonDown("Fire1") && player.ammo !=0 && inGameMenu.InMenu == false)
        {
            GameObject temp_bullet_handler;
            temp_bullet_handler = Instantiate(laser,laseremitter.transform.position,laseremitter.transform.rotation) as GameObject;
            temp_bullet_handler.transform.Rotate(Vector3.left * 90);
            Rigidbody temp_rigidbody;
            temp_rigidbody = temp_bullet_handler.GetComponent<Rigidbody>();
            temp_rigidbody.AddForce(transform.forward * laserforce);
            myAudio.playPlayerFire();
            player.ammo = player.ammo - 1;

            Destroy(temp_bullet_handler, 3f);
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            reload(); 
        }
    }

    void reload()
    {
        myAudio.playReload();
        player.ammo = 15;
    }

   
}
