﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameMenu : MonoBehaviour
{
    //instance of checkplaytype class so i can use the isVRMode variable
    public GameObject InGameMenuUI; //In KM game menu object attached in Unity.
    public GameObject SoundsScreen;
    public GameObject ControlsScreen;
    private bool isOtherUIElementsEnabled; //bool for making sure that the escape keypress cannot bring up the main menu while in any other menu, therby trapping the player.
    public bool InMenu = false;
    public MyAudio myAudio;

    // Use this for initialization
    void Start()
    {
        InGameMenuUI.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && isOtherUIElementsEnabled == false)  //if escape is pressed the menu will be shown.
        {
            showInGameMenu();
        }
    }

    public void showInGameMenu()
    {
        InGameMenuUI.SetActive(true); //Sets the menu object visibility to true, if the editableUI is not toggled already.
        myAudio.stopBacktrack1();
        myAudio.playBacktrack2();
        InMenu = true;
        Cursor.visible = true;
    }

    public void hideInGameMenu()
    {
        myAudio.playMenuButton();
        InGameMenuUI.SetActive(false); //Sets the menu object visibility to false.
        myAudio.stopBacktrack2();
        myAudio.playBacktrack1();
        InMenu = false;
        Cursor.visible = false;
    }

    public void ShowControlsScreen() // this method enables the controls screen object and then disables the ingamemenu object
    {
        myAudio.playMenuButton();
        isOtherUIElementsEnabled = true;
        ControlsScreen.SetActive(true);
        InGameMenuUI.SetActive(false);
    }

    public void ShowSoundsScreen() // this method enables the controls screen object and then disables the ingamemenu object
    {
        myAudio.playMenuButton();
        isOtherUIElementsEnabled = true;
        SoundsScreen.SetActive(true);
        InGameMenuUI.SetActive(false);
    }

    public void ReturnFromSoundsScreen() // this method disenables the controls screen object and then re-enables the ingamemenu object
    {
        myAudio.playMenuButton();
        isOtherUIElementsEnabled = false;
        SoundsScreen.SetActive(false);
        InGameMenuUI.SetActive(true);
    }

    public void ReturnFromControlsScreen() // this method disenables the controls screen object and then re-enables the ingamemenu object
    {
        myAudio.playMenuButton();
        isOtherUIElementsEnabled = false;
        ControlsScreen.SetActive(false);
        InGameMenuUI.SetActive(true);
    }

    public void InGameExitGame()
    {
        //Exits the Game, Only when built as .exe , NOT IN THE EDITOR.
        Application.Quit();
    }
}
