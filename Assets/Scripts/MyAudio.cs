﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class MyAudio : MonoBehaviour
{
    public AudioSource enemyFire;
    public AudioSource playerFire;
    public AudioSource targethit;
    public AudioSource playerdeath;
    public AudioSource playerwalk;
    public AudioSource playerjump;
    public AudioSource menubutton;
    public AudioSource reload;
    public AudioSource backtrack1;
    public AudioSource backtrack2;

    float volume;
    float musicVolume;
    float seVolume;
    public AudioMixer MasterMixer;
    public Timer timer;
    public EndPointCollider endPointCollider;

    public AudioSource guideAngry;
    public AudioSource guide1;
    public AudioSource guide2;
    public AudioSource guideBest;
    // Start is called before the first frame update
    void Start()
    {
        if (SceneManager.GetActiveScene().name == "SemperFiMenuScene")
        {
            playBacktrack2();
        }
        else if (SceneManager.GetActiveScene().name != "SemperFiMenuScene")
        {
            playBacktrack1();
        }
        StartCoroutine(opening());
    }

    // Update is called once per frame
    void Update()
    {
        if (SceneManager.GetActiveScene().name != "SemperFiMenuScene")
        {
            if (timer.secondsCount > 200)
            {
                backtrack1.pitch = 1.5f;
                playguideAngry();
            }
        }
    }

    IEnumerator opening()
    {
        if (SceneManager.GetActiveScene().name != "SemperFiMenuScene")
        {
            endPointCollider.isRestting = true;
            playguide1();
            yield return new WaitForSeconds(3);
            playguide2();
            yield return new WaitForSeconds(4);
            endPointCollider.isRestting = false;
        }
    }

    public void playguideAngry()
    {
        guideAngry.Play(0);
    }
    public void playguide1()
    {
        guide1.Play(0);
    }
    public void playguide2()
    {
        guide2.Play(0);
    }
    public void playguideBest()
    {
        guideBest.Play(0);
    }
    public void playBacktrack1()
    {
        backtrack1.Play(0);
    }
    public void stopBacktrack1()
    {
        backtrack1.Stop();
    }
    public void playBacktrack2()
    {
        backtrack2.Play(0);
    }
    public void stopBacktrack2()
    {
        backtrack2.Stop();
    }
    public void changeVolume(float volume)
    {
        MasterMixer.SetFloat("Volume", volume);
    }
    public void changeMusicVolume(float musicVolume)
    {
        MasterMixer.SetFloat("MusicVolume", musicVolume);
    }
    public void changeSEVolume(float seVolume)
    {
        MasterMixer.SetFloat("SEVolume", seVolume);
    }
    public void playEnemyFire()
    {
        enemyFire.Play(0);
    }
    public void playPlayerFire()
    {
        playerFire.Play(0);
    }
    public void playTargetHit()
    {
        targethit.Play(0);
    }
    public void playPlayerDeath()
    {
        playerdeath.Play(0);
    }
    public void playPlayerWalk()
    {
        playerwalk.Play(0);
    }
    public void playPlayerJump()
    {
        playerjump.Play(0);
    }
    public void playMenuButton()
    {
        menubutton.Play(0);
    }
    public void playReload()
    {
        reload.Play(0);
    }
}
