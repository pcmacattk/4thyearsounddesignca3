﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public GameObject Player;
    public GameObject enemy;
    public int enemyHealth = 1;
    public GameObject enemyGun;
    public bool IsAttacking  = false;
    public GameObject laser;
    public Transform laserspawn;
    public Vector3 Distance = new Vector3();
    public float DistanceFrom ;
    public float fireRate = 0.5f;
    public float nextFire = 0;
    public EndPoint endPoint;
    public MyAudio myAudio;

    void  Update()
    {
        Attacking();

        // Calculate the distance between the player  the enemy

        Distance = (enemy.transform.position - Player.transform.position);
        Distance.y = 0;
        DistanceFrom = Distance.magnitude;
        Distance /= DistanceFrom;

        // If the player is 20m away from the enemy, ATTACK!

        if (DistanceFrom < 20)
        {
            IsAttacking = true;
        }
        else
        {
            IsAttacking = false;
        }

        if (enemyHealth == 0)
        {
            enemyDeath();
        }
    }

    void  Attacking()
    {
        if (IsAttacking)
        {

            // The enemy isn't blind so it should face the player
            enemyGun.transform.LookAt(Player.transform);
            enemy.transform.LookAt(Player.transform);

            //Shoot

            if (Time.time > nextFire)
            {
                nextFire = Time.time + fireRate;

                GameObject temp_bullet_handler;
                AudioSource temp_enemy_fire;
                temp_bullet_handler = Instantiate(laser, laserspawn.transform.position, laserspawn.transform.rotation) as GameObject;

                temp_bullet_handler.transform.Rotate(Vector3.left * 90);
                Rigidbody temp_rigidbody;
                temp_rigidbody = temp_bullet_handler.GetComponent<Rigidbody>();
                temp_rigidbody.AddForce(transform.forward * 5000);
                temp_enemy_fire = Instantiate(myAudio.enemyFire, enemy.transform);
                myAudio.playEnemyFire();
                //DestroyImmediate(myAudio.enemyFire);
                Destroy(temp_bullet_handler, 3f);
            }
        }
    }

    public void enemyTakeDamage()
    {
        enemyHealth = enemyHealth - 1;
    }

    public void enemyDeath()
    {
        endPoint.enemyKilled();
        enemy.SetActive(false);
    }
}
