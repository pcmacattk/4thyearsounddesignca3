﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnEnemys : MonoBehaviour
{
    public GameObject[] allEnemys;
    public GameObject[] allTargets;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void respawnAllEnemysAndTargets()
    {
        for (int i = 0; i < allEnemys.Length ; i++)
        {
            allEnemys[i].GetComponentInChildren<Enemy>().enemyHealth = 1;
            allEnemys[i].gameObject.SetActive(true);
            Debug.Log("Resetting Enemy" + allEnemys[i]);
        }
        for (int i = 0; i < allTargets.Length ; i++)
        {
            allTargets[i].GetComponent<Target>().health = 1;
            allTargets[i].gameObject.SetActive(true);
            Debug.Log("Resetting Targets" + allTargets[i]);
        }

    }
}
