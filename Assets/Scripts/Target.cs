﻿using UnityEngine;

public class Target : MonoBehaviour {
    public float health = 1f;
    public EndPoint endPoint;
    public UIElements uIElements;
    public MyAudio myAudio;

    public void TakeDamage(float damageAmount)
    {
        health -= damageAmount;
        if(health <= 0f)
        {
            Die();
        }
    }

    void Die()
    {
        gameObject.SetActive(false);
    }

   
    public void targetHit()
    {
        endPoint.numOfTargets = endPoint.numOfTargets + 1;
        myAudio.playTargetHit();
    }
}
