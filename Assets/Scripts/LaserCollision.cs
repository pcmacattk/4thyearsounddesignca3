﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserCollision : MonoBehaviour
{
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnCollisionEnter(Collision collision)
    {
        
        //Check for a match with the specified name on any GameObject that collides with your GameObject
        if (collision.gameObject.transform.GetComponent<Target>() == true)
        {
            
            Target target = collision.gameObject.transform.GetComponent<Target>();
            target.targetHit();
            target.TakeDamage(1);
        }

        if (collision.gameObject.transform.GetComponent<Enemy>() == true)
        {
            
            Enemy enemy = collision.gameObject.transform.GetComponent<Enemy>();
            enemy.enemyTakeDamage();
        }
        if (collision.gameObject.transform.name == "Player")
        {
            
            Player player = collision.gameObject.transform.GetComponent<Player>();
            player.gotShot();
        }
    }
}
