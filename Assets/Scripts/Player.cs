﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Player : MonoBehaviour
{
    public int health = 10;
    public UIElements ui;
    public int ammo = 15;
    public GameObject playerObject;
    public TextMeshProUGUI ammoText;
    public Reset reset;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
        {
            respawnReset();
            reset.playerDied();
        }
        ammoText.text = ammo.ToString();
        ui.damageTaken.text = health.ToString();
    }

    public void gotShot()
    {
        health = health - 2;
        
    }
    void respawnReset()
    {
        playerObject.transform.position = new Vector3(106,3.81f,0);
    }
    
}
