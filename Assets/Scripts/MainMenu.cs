﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject SoundsScreen;
    public GameObject ControlsScreen;
    public GameObject mainscreen;
    private bool isOtherUIElementsEnabled; //bool for making sure that the escape keypress cannot bring up the main menu while in any other menu, therby trapping the player.

    void Start()
    {
        
    }

    public void StartGame()
    {
        //Loading the Main Room Scene, (If new scenes are added later on they also must be added in unity build settings as well as these lines of code.)
        SceneManager.LoadScene("SemperFiGameScene");

    }

    public void ExitGame()
    {
        //Exits the Game, Only when built as .exe , NOT IN THE EDITOR.
        Application.Quit();
    }
    public void GoToLoginScreen()
    {
        mainscreen.SetActive(false);
        //loginscreen.SetActive(true);
    }
    public void ShowControlsScreen() // this method enables the controls screen object and then disables the ingamemenu object
    {
        isOtherUIElementsEnabled = true;
        ControlsScreen.SetActive(true);
        mainscreen.SetActive(false);
    }

    public void ShowSoundsScreen() // this method enables the controls screen object and then disables the ingamemenu object
    {
        isOtherUIElementsEnabled = true;
        SoundsScreen.SetActive(true);
        mainscreen.SetActive(false);
    }

    public void ReturnFromSoundsScreen() // this method disenables the controls screen object and then re-enables the ingamemenu object
    {
        isOtherUIElementsEnabled = false;
        SoundsScreen.SetActive(false);
        mainscreen.SetActive(true);
    }

    public void ReturnFromControlsScreen() // this method disenables the controls screen object and then re-enables the ingamemenu object
    {
        isOtherUIElementsEnabled = false;
        ControlsScreen.SetActive(false);
        mainscreen.SetActive(true);
    }
    public void ExitFromLoginScreen()
    {
        //loginscreen.SetActive(false);
        mainscreen.SetActive(true);
    }
}
