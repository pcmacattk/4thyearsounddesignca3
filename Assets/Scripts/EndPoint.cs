﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndPoint : MonoBehaviour
{
    public int numOfEnemys = 0;
    public int numOfTargets  = 0;
    public UIElements uIElements;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void enemyKilled()
    {
        numOfEnemys = numOfEnemys + 1;
        
    }

    public void resetNumbersOfEnemysAndTargets()
    {
        numOfEnemys = 0;
        numOfTargets = 0;
    }
}
